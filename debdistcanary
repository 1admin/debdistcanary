#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

ARCHIVEDIR="$1"
CANARYDIR="$2"
CANARYURL="$3"

# XXX maybe we should not create canary witnesses when PGP verify failed?

mkdir -p $CANARYDIR
for CODENAME in $(find $ARCHIVEDIR -name InRelease | sed -n -e 's,.*/\([^/]*\)/InRelease,\1,p'); do
    RELEASEFILE="$CODENAME/Release"

    test -f "$ARCHIVEDIR/$RELEASEFILE" || continue

    SHA256=`cat $ARCHIVEDIR/$RELEASEFILE | sha256sum - | sed 's,  -,,'`

    printf '%s\n' "Canary: $CANARYURL/$SHA256.witness" "Release: $RELEASEFILE" > $CANARYDIR/$SHA256.witness
    grep '^Date:' $ARCHIVEDIR/$RELEASEFILE | head -1 >> $CANARYDIR/$SHA256.witness

    RELEASEFILE=$CODENAME/InRelease
    # The following should match whatever apt is doing internally:
    #
    # https://salsa.debian.org/apt-team/apt/-/blob/09d4933fa8a472ea3f1efa9484f962b94886c07f/apt-pkg/contrib/gpgv.cc#L418
    #
    # Currently we convert this into:
    # 1. Skip all lines until an empty line.
    # 2. Print all lines until -----BEGIN PGP SIGNATURE-----
    # 3. Remove the final \n
    SHA256=`cat $ARCHIVEDIR/$RELEASEFILE | sed -n '/^$/,$p' | sed 1d | sed -n '/^-----BEGIN PGP SIGNATURE-----$/q;p' | perl -p -e 'chomp if eof' | sha256sum - | sed 's,  -,,'`

    printf '%s\n' "Canary: $CANARYURL/$SHA256.witness" "Release: $RELEASEFILE" > $CANARYDIR/$SHA256.witness
    grep '^Date:' $ARCHIVEDIR/$RELEASEFILE | head -1 >> $CANARYDIR/$SHA256.witness
done

exit 0
